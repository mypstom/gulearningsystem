import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vueMoment from 'vue-moment'
import VueLodash from 'vue-lodash'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// const options = { name: '_' }

Vue.use(VueLodash);
Vue.use(vueMoment);
Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
