import Vue from 'vue'
import VueRouter from 'vue-router'
import BoardGame from '../views/BoardGame.vue'
import Exp from '../views/Exp.vue'
import ExpBeta from '../views/ExpBeta.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'BoardGame',
    component: BoardGame
  },{
    path: '/exp',
    name: 'ExpBeta',
    component: ExpBeta
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
